/**
 * This file contains the Unit tests for the SHA256 and PRESENT-80 implementation.
 */

#include <iostream>
#include <stdio.h>

#include "UnitTest++/UnitTest++.h"
#include "present/present.h"
#include "present/present_encdec.h"
#include "sha2/sha2.h"


#define HASH_KEY_PLAIN_LEN 26

using namespace std;

namespace
{

    /**
     * Check the result of the SHA256 hashing implementation
     */
    TEST(CheckSha256Result)
    {
        uint8_t hashKeyPlain[HASH_KEY_PLAIN_LEN], hashedKey[SHA256_DIGEST_LENGTH];

        cout << "SHA256" << endl << endl;

        // Generate an example of key to be hashed
        for (int i = 0; i < HASH_KEY_PLAIN_LEN; i++)
            hashKeyPlain[i] = (uint8_t)('a'+i);

        cout << "plain hash key: " << endl;
        for (int i = 0; i < HASH_KEY_PLAIN_LEN; i++)
            printf("%c, ", (char)hashKeyPlain[i]);
        cout << endl << endl;

        // Hash of reference, generated with an online calculator
        uint8_t referenceHash[SHA256_DIGEST_LENGTH] = {0x71, 0xc4, 0x80, 0xdf, 0x93, 0xd6, 0xae, 0x2f,
                                                 0x1e, 0xfa, 0xd1, 0x44, 0x7c, 0x66, 0xc9, 0x52,
                                                 0x5e, 0x31, 0x62, 0x18, 0xcf, 0x51, 0xfc, 0x8d,
                                                 0x9e, 0xd8, 0x32, 0xf2, 0xda, 0xf1, 0x8b, 0x73};

        SHA256_CTX ctx;
        SHA256_Init(&ctx);

        SHA256_Update(&ctx, hashKeyPlain, HASH_KEY_PLAIN_LEN);
        SHA256_Final(hashedKey, &ctx);

        cout << "hashed key (length " << SHA256_DIGEST_LENGTH << "): " << endl;
        for (int i = 0; i < SHA256_DIGEST_LENGTH; i++)
            printf("%02x, ", hashedKey[i]);
        cout << endl << endl;


        // Check if the digest is correct
        bool result = true;

        for (int i = 0; i < SHA256_DIGEST_LENGTH; i++)
            if (hashedKey[i] != referenceHash[i])
            {
                result = false;
                break;
            }


        cout << endl << endl << "-----------------------------------------------" << endl << endl;


        CHECK_EQUAL(true, result);
    }

    /**
     * Check the result of the SHA256 hashing implementation with a salt of 4 Bytes
     */
    TEST(CheckSha256WithSaltResult)
    {
        uint8_t hashKeyPlain[HASH_KEY_PLAIN_LEN],
                hashedKey1[SHA256_DIGEST_LENGTH], hashedKey2[SHA256_DIGEST_LENGTH],
                salt1[4] = {0x01, 0x02, 0x03, 0x04}, salt2[4] = {0x05, 0x06, 0x07, 0x08};

        cout << "SHA256 WITH SALT" << endl << endl;

        // Generate an example of key to be hashed
        for (int i = 0; i < HASH_KEY_PLAIN_LEN; i++)
            hashKeyPlain[i] = (uint8_t)('a'+i);

        cout << "plain hash key: " << endl;
        for (int i = 0; i < HASH_KEY_PLAIN_LEN; i++)
            printf("%c, ", (char)hashKeyPlain[i]);
        cout << endl << endl;

        // Hash of reference, generated with an online calculator
        uint8_t referenceHash1[SHA256_DIGEST_LENGTH] = {0xee, 0x5a, 0x1f, 0xde, 0xe7, 0x26, 0x70, 0x82,
                                                        0xbf, 0x90, 0x91, 0x20, 0x51, 0x3b, 0xe9, 0xde,
                                                        0xf2, 0x0b, 0x7d, 0xe3, 0xc6, 0x22, 0x1a, 0x17,
                                                        0x04, 0x0c, 0xe7, 0x00, 0x1d, 0x98, 0x72, 0xc4};

        uint8_t referenceHash2[SHA256_DIGEST_LENGTH] = {0xd8, 0x4e, 0x57, 0xcf, 0x7f, 0x67, 0x8a, 0x9a,
                                                        0x8b, 0x58, 0x94, 0x84, 0x5b, 0x15, 0x33, 0x6a,
                                                        0x40, 0xba, 0xc7, 0x79, 0xec, 0x15, 0x18, 0x65,
                                                        0x9a, 0x09, 0x75, 0x94, 0x26, 0x07, 0x7f, 0xb3};

        sha2(hashKeyPlain, HASH_KEY_PLAIN_LEN, salt1, hashedKey1);

        cout << "hashed key with first salt" << endl;
        for (int i = 0; i < SHA256_DIGEST_LENGTH; i++)
            printf("0x%02x, ", hashedKey1[i]);
        cout << endl << endl;

        sha2(hashKeyPlain, HASH_KEY_PLAIN_LEN, salt2, hashedKey2);

        cout << "hashed key with second salt" << endl;
        for (int i = 0; i < SHA256_DIGEST_LENGTH; i++)
            printf("0x%02x, ", hashedKey2[i]);
        cout << endl << endl;


        // Check if the digest is correct
        bool result = true;

        for (int i = 0; i < SHA256_DIGEST_LENGTH; i++)
            if (hashedKey1[i] != referenceHash1[i])
            {
                result = false;
                break;
            }

        for (int i = 0; i < SHA256_DIGEST_LENGTH && result; i++)
            if (hashedKey2[i] != referenceHash2[i])
            {
                result = false;
                break;
            }


        cout << endl << endl << "-----------------------------------------------" << endl << endl;


        CHECK_EQUAL(true, result);
    }


    /**
     * Check the result of the cipher block with the PRESENT algorithm encryption
     */
    TEST(CheckPresentBlockCipher)
    {
        uint8_t plainBlock[PRESENT_BLOCK_SIZE];
        uint8_t block[PRESENT_BLOCK_SIZE], key[PRESENT_KEY_SIZE] = {0x00};
        uint8_t referenceCipher[PRESENT_BLOCK_SIZE] = {0xfd, 0x37, 0x6a, 0xd0, 0x13, 0x43, 0x78, 0xa3};
        uint64_t block64;
        uint64_t keyhigh, keylow;

        cout << "PRESENT BLOCK" << endl << endl;

        cout << "plain block: " << endl;
        for (int i = 0; i < PRESENT_BLOCK_SIZE; i++)
        {
            plainBlock[i] = i;
            block[i] = i;
            printf("%02x, ", block[i]);
        }
        cout << endl << endl;

        cout << "key used: " << endl;
        for (int i = 0; i < PRESENT_KEY_SIZE; i++)
        {
            key[i] = i;
            printf("%02x, ", key[i]);
        }
        cout << endl << endl;

        presentConvertKeyArrayToUint64(key, &keyhigh, &keylow);

        block64 = Uint8ArrtoUint64(block, 0);

        block64 = presentEncryptBlock(block64, keyhigh, keylow);

        Uint64toUint8Arr(block, block64, 0);

        cout << "cipher block: " << endl;
        for (int i = 0; i < PRESENT_BLOCK_SIZE; i++)
            printf("%02x, ", block[i]);
        cout << endl << endl;

        // Check if the cipher block is correct
        bool result = true;

        for (int i = 0; i < PRESENT_BLOCK_SIZE; i++)
            if (block[i] != referenceCipher[i])
            {
                result = false;
                break;
            }

        // Decrypts the block
        block64 = presentDecryptBlock(block64, keyhigh, keylow);
        Uint64toUint8Arr(block, block64, 0);

        cout << "block decrypted: " << endl;
        for (int i = 0; i < PRESENT_BLOCK_SIZE; i++)
            printf("%02x, ", block[i]);
        cout << endl << endl;

        // Check if the decryption is correct
        for (int i = 0; i < PRESENT_BLOCK_SIZE && result; i++)
            if (block[i] != plainBlock[i])
            {
                result = false;
                break;
            }

        cout << endl << endl << "-----------------------------------------------" << endl << endl;


        CHECK_EQUAL(true, result);
    }

    /**
     * Check the result of a message encryption with the cipher algorithm
     */
    TEST(CheckPresentMessageEncryption)
    {
        const uint8_t messageSize = PRESENT_BLOCK_SIZE*1.5;
        const uint8_t encryptedSize = presentComputeEncryptedSize(messageSize);
        uint8_t plainMessage[messageSize],
                key[PRESENT_KEY_SIZE] = {0x00},
                encryptedMessage[encryptedSize],
                decryptedMessage[encryptedSize],
                iv[PRESENT_IV_SIZE];

        cout << "PRESENT MESSAGE" << endl << endl;

        printf("message size = %d \nencrypted size = %d \n\n", messageSize, encryptedSize);

        cout << "plain message: " << endl;
        for (int i = 0; i < messageSize; i++)
        {
            plainMessage[i] = i;
            printf("%02x, ", plainMessage[i]);
        }
        cout << endl << endl;

        cout << "key used: " << endl;
        for (int i = 0; i < PRESENT_KEY_SIZE; i++)
        {
            key[i] = i;
            printf("%02x, ", key[i]);
        }
        cout << endl << endl;

        cout << "IV used: " << endl;
        for (int i = 0; i < PRESENT_IV_SIZE; i++)
        {
            iv[i] = 3;
            printf("%02x, ", iv[i]);
        }
        cout << endl << endl;

        presentEncrypt(plainMessage, messageSize, key, iv, encryptedMessage);

        cout << "encrypted message: " << endl;
        for (int i = 0; i < encryptedSize; i++)
            printf("%02x, ", encryptedMessage[i]);
        cout << endl << endl;


        // Decrypts the message
        presentDecrypt(encryptedMessage, encryptedSize, key, iv, decryptedMessage);

        cout << "decrypted message: " << endl;
        for (int i = 0; i < encryptedSize; i++)
            printf("%02x, ", decryptedMessage[i]);
        cout << endl << endl;

        // Check if the message decrypted is correct
        bool result = true;
        for (int i = 0; i < messageSize && result; i++)
            if (decryptedMessage[i] != plainMessage[i])
            {
                result = false;
                break;
            }


        cout << endl << endl << "-----------------------------------------------" << endl << endl;

        CHECK_EQUAL(true, result);
    }

}
