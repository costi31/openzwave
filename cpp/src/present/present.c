/**
 * This file was written by Luca Costanzo.
 * It contains two functions to encrypt and decrypt a message of variable
 * size using an 8 Bytes IV and a 10 Bytes Key with the PRESENT-80 algorithm
 * and the Cipher Block Chaining (CBC) mode of operation.\n
 * It contains also other public helper functions and constants.
 */

#include "present.h"
#include "present_encdec.h"
#include <string.h>


void Uint64toUint8Arr (uint8_t* buf, const uint64_t var, const uint32_t lowest_pos)
{
    buf[lowest_pos+7]   =   (var & 0x00000000000000FF) >> 0 ;
    buf[lowest_pos+6]   =   (var & 0x000000000000FF00) >> 8 ;
    buf[lowest_pos+5]   =   (var & 0x0000000000FF0000) >> 16 ;
    buf[lowest_pos+4]   =   (var & 0x00000000FF000000) >> 24 ;
    buf[lowest_pos+3]   =   (var & 0x000000FF00000000) >> 32 ;
    buf[lowest_pos+2]   =   (var & 0x0000FF0000000000) >> 40 ;
    buf[lowest_pos+1]   =   (var & 0x00FF000000000000) >> 48 ;
    buf[lowest_pos]     =   (var & 0xFF00000000000000) >> 56 ;
}


uint64_t Uint8ArrtoUint64 (const uint8_t* var, const uint32_t lowest_pos)
{
    return  (((uint64_t)var[lowest_pos])   << 56) |
            (((uint64_t)var[lowest_pos+1]) << 48) |
            (((uint64_t)var[lowest_pos+2]) << 40) |
            (((uint64_t)var[lowest_pos+3]) << 32) |
            (((uint64_t)var[lowest_pos+4]) << 24) |
            (((uint64_t)var[lowest_pos+5]) << 16) |
            (((uint64_t)var[lowest_pos+6]) << 8)  |
            (((uint64_t)var[lowest_pos+7]) << 0);
}

void presentConvertKeyArrayToUint64 (const uint8_t keyArr[10], uint64_t * keyhigh, uint64_t * keylow)
{
    *keyhigh = Uint8ArrtoUint64(keyArr, 0);
    *keylow = ( ((uint64_t)keyArr[8]) << 8) |
               ((uint64_t)keyArr[9]);
}


uint8_t presentComputeEncryptedSize(const uint8_t messageSize)
{
    uint8_t padding = PRESENT_BLOCK_SIZE - (messageSize % PRESENT_BLOCK_SIZE);
    if (padding == PRESENT_BLOCK_SIZE)
        padding = 0;

    return messageSize + padding;
}


void presentResetBlock (uint8_t * block)
{
    memset(block, PRESENT_PADDING_VAL, PRESENT_BLOCK_SIZE * sizeof(uint8_t));
}


/** \brief Copies a block from sourceBuf to destBuf starting from position sourcePos and destPos of the arrays
 * \param sourceBuf the source array that contains the block to be copied
 * \param sourcePos the starting position of the block in the sourceBuf array
 * \param destBuf the destination array for the copy
 * \param destPos the starting position of the block in the destBuf array
 */
void presentCopyBlock (const uint8_t * sourceBuf, const uint8_t sourcePos, uint8_t * destBuf, const uint8_t destPos)
{
    uint8_t i;

    for (i = 0; i < PRESENT_BLOCK_SIZE; i++) {
        destBuf[destPos+i] = sourceBuf[sourcePos+i];
    }
}


/** \brief Executes the XOR bitwise from each element of block1 and each one of block2 overriding the result directly in block2.
 *
 * \param block1 the first block for the XOR
 * \param block2 the second block for the XOR that is also overriden with the result
 *
 */
void presentXorBlock (const uint8_t * block1, uint8_t * block2)
{
    uint8_t i;

    for (i = 0; i < PRESENT_BLOCK_SIZE; i++) {
        block2[i] ^= block1[i];
    }
}


bool presentEncrypt(const uint8_t * plainMessage, const size_t messageSize, const uint8_t * key, const uint8_t * iv, uint8_t * encryptedMessage)
{
    uint8_t blocksCount = presentComputeEncryptedSize(messageSize) / PRESENT_BLOCK_SIZE;
    uint8_t i; // counter for the block number
    uint8_t j; // counter for the copy of the last Bytes of the plain message when padding is needed
    uint8_t block1[PRESENT_BLOCK_SIZE]; // contains each block of the plain text
    uint8_t block2[PRESENT_BLOCK_SIZE]; // contains the second block to be XORed at each step
    uint64_t block1_64;
    uint8_t xorIv = 1;
    uint64_t keyhigh, keylow; // 64 bit values for the key required by the present implementation
    presentConvertKeyArrayToUint64(key, &keyhigh, &keylow);


    // If the iv is null I don't XOR the plain block with the IV at the first step
    if (iv == 0)
        xorIv = 0;
    else
    // If the iv is not null I copy it into block2 for the first XOR block in the encryption chain
        presentCopyBlock(iv, 0, block2, 0);

    // I encrypts each block of the message with the CBC mode of operation
    for (i = 0; i < blocksCount; i++)
    {
        // I check if I'm in the last block and I need to add the padding to the plain message
        if (i == blocksCount-1 && messageSize < PRESENT_BLOCK_SIZE*blocksCount)
        {
            // I reset the block1, so that it contains only padding values
            presentResetBlock(block1);
            // I override the valid values of block1 with the ones read from the plainMessage
            for (j = i*PRESENT_BLOCK_SIZE; j < messageSize; j++)
                block1[j-i*PRESENT_BLOCK_SIZE] = plainMessage[j];
        }
        else
            // First i copy the current block of the plain message in block1
            presentCopyBlock(plainMessage, i*PRESENT_BLOCK_SIZE, block1, 0);

        // Executes the XOR bitwise from each element of block2 and block1
        if (i > 0 || xorIv == 1)
            presentXorBlock(block2, block1);

        // Encrypts the block
        block1_64 = Uint8ArrtoUint64(block1, 0);
        block1_64 = presentEncryptBlock(block1_64, keyhigh, keylow);
        Uint64toUint8Arr(block1, block1_64, 0);
        block1_64 = 0;

        // Copy the cipher block in the encrypted message
        presentCopyBlock(block1, 0, encryptedMessage, i*PRESENT_BLOCK_SIZE);

        // Copy the cipher block in block2 for the next encryption in the chain
        presentCopyBlock(block1, 0, block2, 0);
    }

    // I resets the block1 and block2 for security reasons
    presentResetBlock(block1);
    presentResetBlock(block2);

    return true;
}

bool presentDecrypt(const uint8_t * encryptedMessage, const size_t messageSize, const uint8_t * key, const uint8_t * iv, uint8_t * plainMessage)
{
    uint8_t blocksCount = messageSize / PRESENT_BLOCK_SIZE;
    uint8_t i; // counter for the block number
    uint8_t block1[PRESENT_BLOCK_SIZE]; // contains each block of the cipher text to be decrypted
    uint8_t block2[PRESENT_BLOCK_SIZE]; // contains the second block to be XORed at each step
    uint64_t block1_64;
    uint8_t blockTemp[PRESENT_BLOCK_SIZE]; // Temporary 64 Byte value that stores cipher text to be used as block2 for the XOR in the next chain step
    uint8_t xorIv = 1;
    uint64_t keyhigh, keylow; // 64 bit values for the key required by the present implementation
    presentConvertKeyArrayToUint64(key, &keyhigh, &keylow);


    // If the iv is null I don't XOR the plain block with the IV at the first step
    if (iv == 0)
        xorIv = 0;
    else
    // If the iv is not null I copy it into block2 for the first XOR block in the encryption chain
        presentCopyBlock(iv, 0, block2, 0);

    // I encrypts each block of the message with the CBC mode of operation
    for (i = 0; i < blocksCount; i++)
    {
        // First i copy the current block of the plain message in block1
        presentCopyBlock(encryptedMessage, i*PRESENT_BLOCK_SIZE, block1, 0);
        presentCopyBlock(encryptedMessage, i*PRESENT_BLOCK_SIZE, blockTemp, 0);

        // Decrypts the block
        block1_64 = Uint8ArrtoUint64(block1, 0);
        block1_64 = presentDecryptBlock(block1_64, keyhigh, keylow);
        Uint64toUint8Arr(block1, block1_64, 0);
        block1_64 = 0;

        // Executes the XOR bitwise from each element of block2 and block1
        if (i > 0 || xorIv == 1)
            presentXorBlock(block2, block1);

        // Copy the plain block in the plain message
        presentCopyBlock(block1, 0, plainMessage, i*PRESENT_BLOCK_SIZE);

        // Copy the previous stored cipher block in block2 for the next decryption in the chain
        presentCopyBlock(blockTemp, 0, block2, 0);
    }

    // I resets the block1 and block2 for security reasons
    presentResetBlock(block1);
    presentResetBlock(block2);

    return true;
}
