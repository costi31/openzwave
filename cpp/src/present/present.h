/**
 * This file was written by Luca Costanzo.
 * It contains two functions to encrypt and decrypt a message of variable
 * size using an 8 Bytes IV and a 10 Bytes Key with the PRESENT-80 algorithm
 * and the Cipher Block Chaining (CBC) mode of operation.\n
 * It contains also other public helper functions and constants.
 */


#ifndef __PRESENT_H__
#define __PRESENT_H__

#include <stdbool.h>
#include <inttypes.h>
#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

#define PRESENT_BLOCK_SIZE 8    /**< Size of a block of Present in Bytes */
#define PRESENT_KEY_SIZE 10     /**< Size of the key in Bytes */
#define PRESENT_IV_SIZE 8       /**< Size of the iv in Bytes */


static const uint8_t PRESENT_PADDING_VAL = 0x00; /**< Byte used for the padding of the message when its size isn't multiple of 8 */


/** \brief Helper function that convert an uint64 to an 8 Bytes array of uint8
 *
 * \param buf the 8 Bytes array of uint8 that will contain the result.
 * \param var the uint64 variable to be converted
 * \param lowest_pos the lowest pos of the array (usually 0)
 *
 */
void Uint64toUint8Arr (uint8_t* buf, const uint64_t var, const uint32_t lowest_pos);


/** \brief Helper function that converts an 8 Bytes array of uint8 to an uint64 value
 *
 * \param var the 8 Bytes array of uint8 to be converted
 * \param lowest_pos the lowest pos of the array (usually 0)
 * \return the uint64 value converted
 *
 */
uint64_t Uint8ArrtoUint64 (const uint8_t* var, const uint32_t lowest_pos);


/** \brief Helper function that converts the key array of 10 uint8 values
 *         to two uint64 keyhigh and keylow values used for the
 *         present block encryption.
 *
 * \param keyArr the key array of 10 uint8 values to be converted
 * \param keyhigh the uint64 variable that will store the first 8 Bytes of the key.
 *                (it has to be passed by address).
 * \param keylow the uint64 variable that will store the remaining 2 Bytes of the key
 *               (the remaining Bytes are zeros, it has to be passed by address).
 *
 */
void presentConvertKeyArrayToUint64 (const uint8_t keyArr[10], uint64_t * keyhigh, uint64_t * keylow);


/** \brief Computes the required size for the encrypted message by taking in consideration
 *         the size of the plain message and the possible padding to
 *         reach a multiple of 8 Bytes in total.
 *
 * \param messageSize the size of the plain message to be encrypted
 * \return the required size for the encrypted message, multiple of 8 Bytes
 *
 */
uint8_t presentComputeEncryptedSize(const uint8_t messageSize);


/** \brief Encrypts a message of variable size with the Present cipher algorithm, using CBC block mode
 *
 * \param plainMessage the plain message to be encrypted
 * \param messageSize the size of the message to be encrypted
 * \param key the 10 Bytes key used for the encryption of each block of the message
 * \param iv the IV used for the encryption in the CBC mode. Set the IV to NULL if you don't want to use it in the first step.
 * \param encryptedMessage the result of the encryption.\n
 *        It must be already initialized with a size greater than plainMessage and multiple of 8.\n
 *        The required size can be computed with the function presentComputeEncryptedSize.
 * \return true if there are no errors
 *
 */
bool presentEncrypt(const uint8_t * plainMessage, const size_t messageSize, const uint8_t * key, const uint8_t * iv, uint8_t * encryptedMessage);


/** \brief Decrypts a message of variable size with the Present cipher algorithm, using ECB block mode
 *
 * \param encryptedMessage the encrypted message to be decrypted
 * \param messageSize the size of the encrypted message
 * \param key the 10 Bytes key used for the decryption of each block of the message
 * \param iv the IV used for the decryption in the CBC mode. Set the IV to NULL if you didn't use the IV for the encryption.
 * \param plainMessage the plain message decrypted.\n
 *        It must be already initialized with the size of encryptedMessage.\n
 *        It contains the message possibly padded with zeros at the end to reach a size multiple of 8 Bytes.
 * \return true if there are no errors
 *
 */
bool presentDecrypt(const uint8_t * encryptedMessage, const size_t messageSize, const uint8_t * key, const uint8_t * iv, uint8_t * plainMessage);


#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif /* __PRESENT_H__ */
