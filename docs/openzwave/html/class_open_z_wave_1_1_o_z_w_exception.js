var class_open_z_wave_1_1_o_z_w_exception =
[
    [ "ExceptionType", "class_open_z_wave_1_1_o_z_w_exception.html#a5a321738af538b5ec245a1cb5cc06e25", [
      [ "OZWEXCEPTION_OPTIONS", "class_open_z_wave_1_1_o_z_w_exception.html#a5a321738af538b5ec245a1cb5cc06e25a1844929a5ba433a06adb57ba163542d7", null ],
      [ "OZWEXCEPTION_CONFIG", "class_open_z_wave_1_1_o_z_w_exception.html#a5a321738af538b5ec245a1cb5cc06e25a5d4a3e0b5819ec374f62bab93fb32c55", null ],
      [ "OZWEXCEPTION_INVALID_HOMEID", "class_open_z_wave_1_1_o_z_w_exception.html#a5a321738af538b5ec245a1cb5cc06e25af6dc8385c379415bf3f66364508417f1", null ],
      [ "OZWEXCEPTION_INVALID_VALUEID", "class_open_z_wave_1_1_o_z_w_exception.html#a5a321738af538b5ec245a1cb5cc06e25a31766216bca7c9f4ec698c8efd59661f", null ],
      [ "OZWEXCEPTION_CANNOT_CONVERT_VALUEID", "class_open_z_wave_1_1_o_z_w_exception.html#a5a321738af538b5ec245a1cb5cc06e25a3794963eafc7846b18cfa4b542df9bc4", null ],
      [ "OZWEXCEPTION_SECURITY_FAILED", "class_open_z_wave_1_1_o_z_w_exception.html#a5a321738af538b5ec245a1cb5cc06e25a5b83da0742ed3ad98c229e9b674b9690", null ]
    ] ],
    [ "OZWException", "class_open_z_wave_1_1_o_z_w_exception.html#a449fd1b0453bf857c58f660edcfb44a5", null ],
    [ "~OZWException", "class_open_z_wave_1_1_o_z_w_exception.html#a891f0d72ac6df696334a47816d8c8223", null ],
    [ "GetFile", "class_open_z_wave_1_1_o_z_w_exception.html#a3f57b82e67ed191ea52aa78a5521f6a3", null ],
    [ "GetLine", "class_open_z_wave_1_1_o_z_w_exception.html#a42a3ed07454397aac7f1b308c7f2ce8d", null ],
    [ "GetMsg", "class_open_z_wave_1_1_o_z_w_exception.html#ae2c0ad919b6db0835dc95dbfd77538ff", null ],
    [ "GetType", "class_open_z_wave_1_1_o_z_w_exception.html#ad6e7cc7764d05294f00947f6d9924631", null ]
];