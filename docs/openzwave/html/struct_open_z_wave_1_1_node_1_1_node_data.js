var struct_open_z_wave_1_1_node_1_1_node_data =
[
    [ "m_averageRequestRTT", "struct_open_z_wave_1_1_node_1_1_node_data.html#a730bd75d08a71ed5d9bb50c58c060e37", null ],
    [ "m_averageResponseRTT", "struct_open_z_wave_1_1_node_1_1_node_data.html#a4e24e64a8ecefef4f43a7d3588165f4d", null ],
    [ "m_ccData", "struct_open_z_wave_1_1_node_1_1_node_data.html#aa4ea526298416054eb08122a2c07d769", null ],
    [ "m_lastReceivedMessage", "struct_open_z_wave_1_1_node_1_1_node_data.html#aeda912a860b24f3b9e99da74ba401544", null ],
    [ "m_lastRequestRTT", "struct_open_z_wave_1_1_node_1_1_node_data.html#a40d0eb71349969c2dfe442ff4024d2fc", null ],
    [ "m_lastResponseRTT", "struct_open_z_wave_1_1_node_1_1_node_data.html#a3870e486d636bfc169d222ea0d6f0621", null ],
    [ "m_quality", "struct_open_z_wave_1_1_node_1_1_node_data.html#aa7c561346bf6685a2ba1222d3af34225", null ],
    [ "m_receivedCnt", "struct_open_z_wave_1_1_node_1_1_node_data.html#ae46baf523f13d521c534115c6f5d9ca1", null ],
    [ "m_receivedDups", "struct_open_z_wave_1_1_node_1_1_node_data.html#a5b00a5e8302e78d1ac97bf583acfda2f", null ],
    [ "m_receivedTS", "struct_open_z_wave_1_1_node_1_1_node_data.html#a795bc47830dffe88b24c69925f5f496c", null ],
    [ "m_receivedUnsolicited", "struct_open_z_wave_1_1_node_1_1_node_data.html#a3805e2abf57f0a0039ca78ea1258ef42", null ],
    [ "m_retries", "struct_open_z_wave_1_1_node_1_1_node_data.html#a1ac876c30a25a60db1f8382230608570", null ],
    [ "m_sentCnt", "struct_open_z_wave_1_1_node_1_1_node_data.html#a031714d9c7fb0975a5a805ca6e8918f6", null ],
    [ "m_sentFailed", "struct_open_z_wave_1_1_node_1_1_node_data.html#a4cef0e18a6142ca03ca0874db6f06831", null ],
    [ "m_sentTS", "struct_open_z_wave_1_1_node_1_1_node_data.html#a8a63438e4ef20cac498c202561518173", null ]
];