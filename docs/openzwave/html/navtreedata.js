var NAVTREE =
[
  [ "OpenZWave Library", "index.html", [
    [ "OpenZWave", "index.html", [
      [ "Introduction to OpenZWave", "index.html#Introduction", null ],
      [ "Z-Wave Concepts", "index.html#ZWave", null ],
      [ "The OpenZWave Library", "index.html#Library", [
        [ "Overview", "index.html#Overview", null ],
        [ "The Manager", "index.html#Manager", null ],
        [ "Notifications", "index.html#Notifications", null ],
        [ "Working with Values", "index.html#Values", null ],
        [ "Application Development Guidelines", "index.html#Guidelines", null ]
      ] ],
      [ "Source Code Structure", "index.html#Structure", null ],
      [ "Samples", "index.html#Samples", null ],
      [ "Licensing", "index.html#Licensing", null ],
      [ "Support", "index.html#Support", null ]
    ] ],
    [ "Todo List", "todo.html", null ],
    [ "Deprecated List", "deprecated.html", null ],
    [ "Namespaces", null, [
      [ "Namespace List", "namespaces.html", "namespaces" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ],
        [ "Typedefs", "namespacemembers_type.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ],
        [ "Enumerator", "namespacemembers_eval.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", "functions_eval" ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", null ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", "globals_eval" ],
        [ "Macros", "globals_defs.html", "globals_defs" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_alarm_8cpp.html",
"_defs_8h.html#a3df377b13268ebb1a97db94f8c6a4347",
"_event_8h.html",
"_sensor_alarm_8cpp.html#ab7eedf267d34c4a13c79dfaba0fd6d0da8c512046fa565dec5088ba1fe8bb9af7",
"_value_list_8h.html",
"aestab_8c.html#a36d8d2d87cfb016918c8906c8db7ae34",
"class_open_z_wave_1_1_command_class.html#a948bccedad6719504f435e17ee755efba27e90de0c1fba2de1a83bb04195cccde",
"class_open_z_wave_1_1_lock.html",
"class_open_z_wave_1_1_msg.html#a9a66f8cf1f60473a55d76b55c8fb9c2b",
"class_open_z_wave_1_1_notification.html#a5fa14ba721a25a4c84e0fbbedd767d54ad935a4a705ce44e9f1d13e88c6b22a77",
"class_open_z_wave_1_1_thermostat_fan_mode.html#aa8e8b04c830b99173538258a33e73bba",
"class_open_z_wave_1_1_value_list.html#a824f8126865d6bb871f6740c83ab460a",
"globals_eval_t.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';