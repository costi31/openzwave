var _color_8cpp =
[
    [ "ColorCmd", "_color_8cpp.html#ab9e43d81fd054619ee8aaf2a585e761e", [
      [ "ColorCmd_Capability_Get", "_color_8cpp.html#ab9e43d81fd054619ee8aaf2a585e761eafb7cace44651ca3a467e2c5ff095b7da", null ],
      [ "ColorCmd_Capability_Report", "_color_8cpp.html#ab9e43d81fd054619ee8aaf2a585e761eaf95e15c695662f2f0c5bcd1705631db2", null ],
      [ "ColorCmd_Get", "_color_8cpp.html#ab9e43d81fd054619ee8aaf2a585e761eac194163d4343b3fd4ff222511b1b70b7", null ],
      [ "ColorCmd_Report", "_color_8cpp.html#ab9e43d81fd054619ee8aaf2a585e761ea188c6e5859157de3302c828fe01ec241", null ],
      [ "ColorCmd_Set", "_color_8cpp.html#ab9e43d81fd054619ee8aaf2a585e761ea6896eb768add88e4a7e955e4e74d9615", null ],
      [ "ColorCmd_StartCapabilityLevelChange", "_color_8cpp.html#ab9e43d81fd054619ee8aaf2a585e761eac3add78730a896ca1828c01aa2771b84", null ],
      [ "ColorCmd_StopStateChange", "_color_8cpp.html#ab9e43d81fd054619ee8aaf2a585e761ea2f97355e6aa93e5235e587514636b76a", null ]
    ] ],
    [ "ColorIDX", "_color_8cpp.html#a1300711d45d00fc635d36fdc1d6bd5fa", [
      [ "COLORIDX_WARMWHITE", "_color_8cpp.html#a1300711d45d00fc635d36fdc1d6bd5faa241e0e8ecd29394dee7a8e118c5eedd8", null ],
      [ "COLORIDX_COLDWHITE", "_color_8cpp.html#a1300711d45d00fc635d36fdc1d6bd5faa712749cf4a0f8c92a34a003c52441daf", null ],
      [ "COLORIDX_RED", "_color_8cpp.html#a1300711d45d00fc635d36fdc1d6bd5faa309858c95102473445924558a5678718", null ],
      [ "COLORIDX_GREEN", "_color_8cpp.html#a1300711d45d00fc635d36fdc1d6bd5faa999f2f945ed8c238b56b057df8e572eb", null ],
      [ "COLORIDX_BLUE", "_color_8cpp.html#a1300711d45d00fc635d36fdc1d6bd5faafda7266ebec9c670a7c7aaf76d43a8aa", null ],
      [ "COLORIDX_AMBER", "_color_8cpp.html#a1300711d45d00fc635d36fdc1d6bd5faac26684069229fa19105cf2c3de7d3296", null ],
      [ "COLORIDX_CYAN", "_color_8cpp.html#a1300711d45d00fc635d36fdc1d6bd5faa91d35852b5060014f207a28a015f22c4", null ],
      [ "COLORIDX_PURPLE", "_color_8cpp.html#a1300711d45d00fc635d36fdc1d6bd5faa8b96b332d90e3ed9b02e0e71857b6231", null ],
      [ "COLORIDX_INDEXCOLOR", "_color_8cpp.html#a1300711d45d00fc635d36fdc1d6bd5faa8a0282cb11a1a67710502c66ab1f4879", null ]
    ] ],
    [ "ValueIDSystemIndexes", "_color_8cpp.html#a0dbbcf84898fc293c614dc1fd24eef3f", [
      [ "Value_Color", "_color_8cpp.html#a0dbbcf84898fc293c614dc1fd24eef3fa3da691fab2cd94e1783b6fa6c9efaa8d", null ],
      [ "Value_Color_Index", "_color_8cpp.html#a0dbbcf84898fc293c614dc1fd24eef3fa962d2f62623e8c2104d85ec422ed72c5", null ],
      [ "Value_Color_Channels_Capabilities", "_color_8cpp.html#a0dbbcf84898fc293c614dc1fd24eef3fa5264e799863a8daa49f68e80e13b2b37", null ],
      [ "Value_Color_Duration", "_color_8cpp.html#a0dbbcf84898fc293c614dc1fd24eef3fafccab39d6ed9a2aaddece5467f387260", null ],
      [ "Value_Lock", "_door_lock_8cpp.html#a0dbbcf84898fc293c614dc1fd24eef3fad5092d71ee2d58574a748978f53b1998", null ],
      [ "Value_Lock_Mode", "_door_lock_8cpp.html#a0dbbcf84898fc293c614dc1fd24eef3fa1d13b08cd8722568d0ffdec0306f4410", null ],
      [ "Value_System_Config_Mode", "_door_lock_8cpp.html#a0dbbcf84898fc293c614dc1fd24eef3fa8bd1bcbdbbe1f6713302f232494f0ac9", null ],
      [ "Value_System_Config_Minutes", "_door_lock_8cpp.html#a0dbbcf84898fc293c614dc1fd24eef3fa0ed985152852f22feac659f453292452", null ],
      [ "Value_System_Config_Seconds", "_door_lock_8cpp.html#a0dbbcf84898fc293c614dc1fd24eef3fa553cb9250e46910700d75f5679cfcdcb", null ],
      [ "Value_System_Config_OutsideHandles", "_door_lock_8cpp.html#a0dbbcf84898fc293c614dc1fd24eef3fae5ee549c8a89f3c909942680a12dc3de", null ],
      [ "Value_System_Config_InsideHandles", "_door_lock_8cpp.html#a0dbbcf84898fc293c614dc1fd24eef3fae35e92b0e3ee346d7f75cfc0293694b7", null ],
      [ "Value_System_Config_MaxRecords", "_door_lock_logging_8cpp.html#a0dbbcf84898fc293c614dc1fd24eef3facb95915cad426f354a68851ed19932be", null ],
      [ "Value_GetRecordNo", "_door_lock_logging_8cpp.html#a0dbbcf84898fc293c614dc1fd24eef3fa5ba0c6b62bcc085785e96e80afc469b7", null ],
      [ "Value_LogRecord", "_door_lock_logging_8cpp.html#a0dbbcf84898fc293c614dc1fd24eef3fab1ccae5f99f62c4202e88cbabf598570", null ]
    ] ],
    [ "GetColor", "_color_8cpp.html#a57b6e745c8b13835fe0f220092bcb00e", null ]
];