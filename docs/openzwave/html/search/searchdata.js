var indexSectionsWithContent =
{
  0: "_abcdefghiklmnopqrstuvwxz~",
  1: "abcdefghilmnoprstuvwz",
  2: "os",
  3: "abcdefghilmnoprstuvwz",
  4: "abcdefghiklmnopqrstuvw~",
  5: "_bcgiklmoru",
  6: "cfhiopru",
  7: "abcdehilmnopqstuvwz",
  8: "abcdehilmnopqrstuvwz",
  9: "abcdefghilmnpstvwz",
  10: "_abcdefghiklmnoprstuvwxz",
  11: "dot"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "related",
  10: "defines",
  11: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Friends",
  10: "Macros",
  11: "Pages"
};

