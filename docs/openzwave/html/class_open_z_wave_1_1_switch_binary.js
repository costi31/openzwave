var class_open_z_wave_1_1_switch_binary =
[
    [ "~SwitchBinary", "class_open_z_wave_1_1_switch_binary.html#a7dd5c2eeeb0baa1df721768d1eee65fb", null ],
    [ "CreateVars", "class_open_z_wave_1_1_switch_binary.html#a9410ba55db9ddc1b9bc232eef09af15d", null ],
    [ "GetCommandClassId", "class_open_z_wave_1_1_switch_binary.html#a7dc1566e9943710c2dda56b301502ff6", null ],
    [ "GetCommandClassName", "class_open_z_wave_1_1_switch_binary.html#a2f06f59917a22cf0b1a5c350dce448a2", null ],
    [ "HandleMsg", "class_open_z_wave_1_1_switch_binary.html#a75ecfd078d9777050d0e547c271a3f53", null ],
    [ "RequestState", "class_open_z_wave_1_1_switch_binary.html#a6c34eaa49924809c9aa65b0850fc31c4", null ],
    [ "RequestValue", "class_open_z_wave_1_1_switch_binary.html#a292265baa9707f825975b911dd2e515c", null ],
    [ "SetValue", "class_open_z_wave_1_1_switch_binary.html#aac14f9eace86217edb2f24b0c9cd9cce", null ],
    [ "SetValueBasic", "class_open_z_wave_1_1_switch_binary.html#a201da798dbec38a206b1ab7b86405396", null ]
];