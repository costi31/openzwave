var struct_open_z_wave_1_1_driver_1_1_driver_data =
[
    [ "m_ACKCnt", "struct_open_z_wave_1_1_driver_1_1_driver_data.html#aa92e2d305833a73b429add4eae4a4ab1", null ],
    [ "m_ACKWaiting", "struct_open_z_wave_1_1_driver_1_1_driver_data.html#ae359845dd0975831101fb9bc440e1c6f", null ],
    [ "m_badChecksum", "struct_open_z_wave_1_1_driver_1_1_driver_data.html#ab4bea5304ad2a2fa2f15d86c03a1f786", null ],
    [ "m_badroutes", "struct_open_z_wave_1_1_driver_1_1_driver_data.html#a182b6c05fda694d6f51e9d4588bb6126", null ],
    [ "m_broadcastReadCnt", "struct_open_z_wave_1_1_driver_1_1_driver_data.html#acd7da01c841922df3c38b35af1f9d26c", null ],
    [ "m_broadcastWriteCnt", "struct_open_z_wave_1_1_driver_1_1_driver_data.html#a552e67ea1b40fc22e8b20e1a9790e763", null ],
    [ "m_callbacks", "struct_open_z_wave_1_1_driver_1_1_driver_data.html#a85a4e018f0a2b01fc2a1d5add8c6d909", null ],
    [ "m_CANCnt", "struct_open_z_wave_1_1_driver_1_1_driver_data.html#a4c1f235ab2dca1b2eb11ff9337198cd4", null ],
    [ "m_dropped", "struct_open_z_wave_1_1_driver_1_1_driver_data.html#a94396f3b14b87a71703aaed74444c664", null ],
    [ "m_NAKCnt", "struct_open_z_wave_1_1_driver_1_1_driver_data.html#afef99ae6e6f49057fe20190dbd01b2e0", null ],
    [ "m_netbusy", "struct_open_z_wave_1_1_driver_1_1_driver_data.html#a2344f833555c3dedfdafb367558b061e", null ],
    [ "m_noack", "struct_open_z_wave_1_1_driver_1_1_driver_data.html#a040ee12013ce529651bded9f63458d8d", null ],
    [ "m_nondelivery", "struct_open_z_wave_1_1_driver_1_1_driver_data.html#a74341c56f573c344845db01e4f07b70c", null ],
    [ "m_notidle", "struct_open_z_wave_1_1_driver_1_1_driver_data.html#ae5d9885fdadd8286bc46ec29f785a4ce", null ],
    [ "m_OOFCnt", "struct_open_z_wave_1_1_driver_1_1_driver_data.html#ac088fff47d5f256e149f2608eaca6249", null ],
    [ "m_readAborts", "struct_open_z_wave_1_1_driver_1_1_driver_data.html#a37aa828df8ba8ded0dcceb476fde6833", null ],
    [ "m_readCnt", "struct_open_z_wave_1_1_driver_1_1_driver_data.html#adb29c8db45c8cf3c44a56adcab7a0a89", null ],
    [ "m_retries", "struct_open_z_wave_1_1_driver_1_1_driver_data.html#a0a348207d24add529b067f689b76cee8", null ],
    [ "m_routedbusy", "struct_open_z_wave_1_1_driver_1_1_driver_data.html#a87a117623ab3dda5da61aae56340f395", null ],
    [ "m_SOFCnt", "struct_open_z_wave_1_1_driver_1_1_driver_data.html#aaff2ccf40597b90c9ea17a40515282a2", null ],
    [ "m_writeCnt", "struct_open_z_wave_1_1_driver_1_1_driver_data.html#a75314211869d80979e4fc36b19264436", null ]
];