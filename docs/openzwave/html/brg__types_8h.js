var brg__types_8h =
[
    [ "ALIGN_CEIL", "brg__types_8h.html#a383bd27575c703ea44e088b854d6815c", null ],
    [ "ALIGN_FLOOR", "brg__types_8h.html#adb1cd2e56899939616dc5d394d7674b6", null ],
    [ "ALIGN_OFFSET", "brg__types_8h.html#a2d6cc075af3f22887b6ac19303a65591", null ],
    [ "BRG_UI32", "brg__types_8h.html#a8668968f5fc6842420f0a1f30055e903", null ],
    [ "BUFR_TYPEDEF", "brg__types_8h.html#a3be87db520f930d257c86f67a725d510", null ],
    [ "INT_RETURN", "brg__types_8h.html#af808374fe222f29598b82d8c1e69efde", null ],
    [ "ptrint_t", "brg__types_8h.html#adb1245afa30b2b4c7a15344cbfec026d", null ],
    [ "RETURN_VALUES", "brg__types_8h.html#a6ab2e13cff777db6a91360d9cae4ee5a", null ],
    [ "UI_TYPE", "brg__types_8h.html#a5c6a745711a986e990adc49d8f9cc8f2", null ],
    [ "UNIT_CAST", "brg__types_8h.html#a221ff2653aee777a3dbeb4a50723bbc7", null ],
    [ "UNIT_TYPEDEF", "brg__types_8h.html#ae9162ac61f95eed65448a056a4ec1af2", null ],
    [ "UPTR_CAST", "brg__types_8h.html#a25a98233435df8c33fa09bf6394c3690", null ],
    [ "VOID_RETURN", "brg__types_8h.html#af71e197a4d5c1137fdca6be3595fdc8a", null ]
];